# Build stage
FROM golang:alpine as builder
RUN apk add --no-cache git npm yarn
WORKDIR /build
COPY src /build

RUN yarn install --pure-lockfile --production && \
    yarn cache clean
RUN mkdir -p assets/dist/js assets/dist/css && \
    cp /build/node_modules/admin-lte/dist/js/adminlte.min.js \
    assets/dist/js/adminlte.min.js && \
    cp /build/node_modules/admin-lte/dist/css/adminlte.min.css \
    assets/dist/css/adminlte.min.css
RUN mkdir -p assets/plugins && \
    cp -r /build/node_modules/admin-lte/plugins/jquery/ \
    /build/node_modules/admin-lte/plugins/fontawesome-free/ \
    /build/node_modules/admin-lte/plugins/bootstrap/ \
    /build/node_modules/admin-lte/plugins/icheck-bootstrap/ \
    /build/node_modules/admin-lte/plugins/toastr/ \
    /build/node_modules/admin-lte/plugins/jquery-validation/ \
    /build/node_modules/admin-lte/plugins/select2/ \
    /build/node_modules/jquery-tags-input/ \
    assets/plugins/
RUN cp -r /build/custom/ assets/

RUN go mod download && \
    go get github.com/GeertJohan/go.rice && \
    go install github.com/GeertJohan/go.rice && \
    go get github.com/GeertJohan/go.rice/rice && \
    go install github.com/GeertJohan/go.rice/rice
RUN rice embed-go && \
    GOFLAGS="-buildvcs=false" CGO_ENABLED=0 GOOS=linux go build -a -o wg-ui . && \
    chmod +x wg-ui

FROM alpine
ENV LOGIN_PAGE=1 \
    BIND_ADDRESS=0.0.0.0:5000
RUN apk --no-cache add ca-certificates wireguard-tools ip6tables
RUN mkdir /app /db
COPY --from=builder /build/wg-ui /app
COPY entrypoint.sh wg-restart /app/
ENTRYPOINT ["/app/entrypoint.sh"]
